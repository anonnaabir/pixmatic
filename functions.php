<?php


			if ( ! defined( 'ABSPATH' ) ) {
				exit; // Exit if accessed directly.
			}

			define( 'WP_PIXMATIC_VERSION', '1.0' );

			if ( ! isset( $content_width ) ) {
				$content_width = 800; // Pixels.
			}

			
			require get_template_directory() . '/admin.php';   // Script Enqueue Functions


			if ( ! function_exists( 'wp_pixmatic_setup' ) ) {
	
				// After Theme Setup

				function wp_pixmatic_setup() {
					$hook_result = apply_filters_deprecated( 'elementor_hello_theme_load_textdomain', [ true ], '2.0', 'wp_pixmatic_load_textdomain' );
					if ( apply_filters( 'wp_pixmatic_load_textdomain', $hook_result ) ) {
						load_theme_textdomain( 'wp-pixmatic', get_template_directory() . '/languages' );
					}

					$hook_result = apply_filters_deprecated( 'elementor_hello_theme_register_menus', [ true ], '2.0', 'wp_pixmatic_register_menus' );
					if ( apply_filters( 'wp_pixmatic_register_menus', $hook_result ) ) {
						register_nav_menus( array( 'menu-1' => __( 'Primary', 'wp-pixmatic' ) ) );
					}

					$hook_result = apply_filters_deprecated( 'elementor_hello_theme_add_theme_support', [ true ], '2.0', 'wp_pixmatic_add_theme_support' );
					if ( apply_filters( 'wp_pixmatic_add_theme_support', $hook_result ) ) {
						add_theme_support( 'post-thumbnails' );
						add_theme_support( 'automatic-feed-links' );
						add_theme_support( 'title-tag' );
						add_theme_support(
							'html5',
							array(
								'search-form',
								'comment-form',
								'comment-list',
								'gallery',
								'caption',
							)
						);
						add_theme_support(
							'custom-logo',
							array(
								'height'      => 100,
								'width'       => 350,
								'flex-height' => true,
								'flex-width'  => true,
							)
						);

						/*
						* Editor Style.
						*/
						add_editor_style( 'editor-style.css' );

						/*
						* WooCommerce.
						*/
						$hook_result = apply_filters_deprecated( 'elementor_hello_theme_add_woocommerce_support', [ true ], '2.0', 'wp_pixmatic_add_woocommerce_support' );
						if ( apply_filters( 'wp_pixmatic_add_woocommerce_support', $hook_result ) ) {
							// WooCommerce in general.
							add_theme_support( 'woocommerce' );
							// Enabling WooCommerce product gallery features (are off by default since WC 3.0.0).
							// zoom.
							add_theme_support( 'wc-product-gallery-zoom' );
							// lightbox.
							add_theme_support( 'wc-product-gallery-lightbox' );
							// swipe.
							add_theme_support( 'wc-product-gallery-slider' );
						}
					}
				}
			}

			add_action( 'after_setup_theme', 'wp_pixmatic_setup' );
			


			// Enqueue All Style and Scripts

			if ( ! function_exists( 'wp_pixmatic_enqueue_scripts' ) ) {

				function wp_pixmatic_enqueue_scripts() {
					wp_enqueue_style("core", get_template_directory_uri() . '/core.css');
					wp_enqueue_style("bootstrap", get_template_directory_uri() . '/includes/css/bootstrap.min.css');
			}

			add_action( 'wp_enqueue_scripts', 'wp_pixmatic_enqueue_scripts' );



			if ( ! function_exists( 'wp_pixmatic_register_elementor_locations' ) ) {
				/**
				 * Register Elementor Locations.
				 *
				 * @param ElementorPro\Modules\ThemeBuilder\Classes\Locations_Manager $elementor_theme_manager theme manager.
				 *
				 * @return void
				 */
				function wp_pixmatic_register_elementor_locations( $elementor_theme_manager ) {
					$hook_result = apply_filters_deprecated( 'elementor_hello_theme_register_elementor_locations', [ true ], '2.0', 'wp_pixmatic_register_elementor_locations' );
					if ( apply_filters( 'wp_pixmatic_register_elementor_locations', $hook_result ) ) {
						$elementor_theme_manager->register_all_core_location();
					}
				}
			}

			add_action( 'elementor/theme/register_locations', 'wp_pixmatic_register_elementor_locations' );



			// Added Custom Favicon Support

			function wp_pixmatic_add_favicon() {

			$options = get_option( 'pixmatic_options' ); // unique id of the framework
			
				echo'<link rel="icon" href="'.$options['pixmatic-custom-favicon']['url'].'" type="image/png" sizes="16x16">';
						
			}
							
			add_action('wp_head', 'wp_pixmatic_add_favicon');
			
			


			if ( ! function_exists( 'wp_pixmatic_body_open' ) ) {
				function wp_pixmatic_body_open() {

					if ( function_exists( 'wp_body_open' ) ) {
						wp_body_open();
					} else {
						do_action( 'wp_body_open' );
					}
				
				}
			}
		}