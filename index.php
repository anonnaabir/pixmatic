<?php

		if ( ! defined( 'ABSPATH' ) ) {
			exit; // Exit if accessed directly.
		}

		get_header();

		$is_pixmatic_exists = function_exists( 'pixmatic_location' );

		if ( is_singular() ) {
			if ( ! $is_pixmatic_exists || ! pixmatic_location( 'single' ) ) {
				get_template_part( 'template-parts/single' );
			}

		} elseif ( is_archive() || is_home() ) {
			if ( ! $is_pixmatic_exists || ! pixmatic_location( 'archive' ) ) {
				get_template_part( 'template-parts/archive' );
			}

		} elseif ( is_search() ) {
			if ( ! $is_pixmatic_exists || ! pixmatic_location( 'archive' ) ) {
				get_template_part( 'template-parts/search' );
			}

		} else {
			if ( ! $is_pixmatic_exists || ! pixmatic_location( 'single' ) ) {
				get_template_part( 'template-parts/404' );
			}
			
		}

		get_footer();
