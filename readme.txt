=== Hello Elementor ===

Contributors: elemntor, KingYes, ariel.k, jzaltzberg, mati1000, bainternet
Requires at least: 4.7
Tested up to: 5.4
Stable tag: 2.3.0
Version: 2.3.0
Requires PHP: 5.4
License: GNU General Public License v3 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.html
Tags: custom-menu, custom-logo, featured-images, rtl-language-support, threaded-comments, translation-ready

A lightweight, plain-vanilla theme for Elementor page builder.

***Hello Elementor*** is distributed under the terms of the GNU GPL v3 or later.

== Description ==

A basic, plain-vanilla, lightweight theme, best suited for building your site using Elementor page builder.

This theme resets the WordPress environment and prepares it for smooth operation of Elementor.

Screenshot's images & icons are licensed under: Creative Commons (CC0), https://creativecommons.org/publicdomain/zero/1.0/legalcode

== Installation ==

1. In your site's admin panel, go to Appearance > Themes and click `Add New`.
2. Type "Hello Elementor" in the search field.
3. Click `Install` and then `Activate` to start using the theme.
4. Navigate to Appearance > Customize in your admin panel and customize to your needs.
5. A notice box may appear, recommending you to install Elementor Page Builder Plugin. You can either use it or any other editor.
6. Create a new page, click `Edit with Elementor`.
7. Once the Elementor Editor is launched, click on the library icon, pick one of the many ready-made templates and click `Insert`.
8. Edit the page content as you wish, you can add, remove and manipulate any of the elements.
9. Enjoy :)

== Customizations ==

Most users will not need to edit the files for customizing this theme.
To customize your site's appearance, simply use ***Elementor***.

However, if you have a particular need to adapt this theme, please read on.

= Style & Stylesheets =

All of your site's styles should be handled directly inside ***Elementor***.
You should not need to edit the SCSS files in this theme in ordinary circumstances.

However, if for some reason there is still a need to add or change the site's CSS, please use a child theme.

= Hooks =

To prevent the loading of any of the these settings, use the following as boilerplate and add the code to your child-theme `functions.php`:
```php
add_filter( 'choose-from-the-list-below', '__return_false' );
```

* `wp_pixmatic_enqueue_style`                 enqueue style
* `wp_pixmatic_enqueue_theme_style`           load theme-specific style (default: load)
* `wp_pixmatic_load_textdomain`               load theme's textdomain
* `wp_pixmatic_register_menus`                register the theme's default menu location
* `wp_pixmatic_add_theme_support`             register the various supported features
* `wp_pixmatic_add_woocommerce_support`       register woocommerce features, including product-gallery zoom, swipe & lightbox features
* `wp_pixmatic_register_elementor_locations`  register elementor settings
* `wp_pixmatic_content_width`                 set default content width to 800px
* `wp_pixmatic_page_title`                    show\hide page title (default: show)
* `wp_pixmatic_viewport_content`              modify `content` of `viewport` meta in header