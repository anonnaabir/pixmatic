<?php


		if ( ! defined( 'ABSPATH' ) ) {
			exit; // Exit if accessed directly.
		}
		?>

		<?php
		while ( have_posts() ) : the_post();
			?>

		<main <?php post_class( 'site-main' ); ?> role="main">

				<header class="masthead">			
						<div class="container">
							<div class="row">
								<div class="col-lg-8 col-md-10 mx-auto">
									<div class="page-heading">
									<?php the_title( '<h1>', '</h1>' ); ?>
								</div>
							</div>
						</div>
					</div>
				</header>
					
						<div class="container">
								<div class="row">
									<div class="col-lg-8 col-md-10 mx-auto">
							<?php the_content(); ?>
						</div>
						</div>
					</div>
			</main>

			<?php
		endwhile;
