<?php

	function wp_pixmatic_fail_load_admin_notice() {
		// Leave to Elementor Pro to manage this.
		if ( function_exists( 'elementor_pro_load_plugin' ) ) {
			return;
	}
}