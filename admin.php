    <?php

    require_once get_theme_file_path() .'/includes/framework/csf/codestar-framework.php'; // Codestar Framework Loaded
    
        if( class_exists( 'CSF' ) ) {

            $prefix = 'pixmatic_options';
        

            CSF::createOptions( $prefix, array(
            'menu_title' => 'Pixmatic Options',
            'menu_slug'  => 'pixmatic-options',
            'framework_title' => 'Pixmatic Options',
            ) );
        

            CSF::createSection( $prefix, array(
            'title'  => 'Typography Controls',
            'fields' => array(
        
                array(
                    'id'      => 'pixmatic-heading-typography',
                    'type'    => 'typography',
                    'title'   => 'H1 Typography',
                    'output'  => 'h1',
                    'default' => array(
                      'color'          => '#ffbc00',
                      'font-family'    => 'Open Sans',
                      'font-size'      => '16',
                      'line-height'    => '20',
                      'letter-spacing' => '-1',
                      'text-align'     => 'left',
                      'subset'         => 'latin-ext',
                      'type'           => 'google',
                      'unit'           => 'px',
                    ),
                  ),

                  array(
                    'id'      => 'pixmatic-h2-typography',
                    'type'    => 'typography',
                    'title'   => 'H2 Typography',
                    'output'  => 'h2',
                    'default' => array(
                        'color'          => '#212529',
                        'font-family'    => 'Open Sans',
                      'font-size'      => '28',
                      'line-height'    => '20',
                      'letter-spacing' => '-1',
                    //   'text-align'     => 'left',
                    //   'subset'         => 'latin-ext',
                      'type'           => 'google',
                      'unit'           => 'px',
                    ),
                  ),

                  array(
                    'id'      => 'pixmatic-h3-typography',
                    'type'    => 'typography',
                    'title'   => 'H3 Typography',
                    'output'  => 'h3',
                    'default' => array(
                      'color'          => '#212529',
                      'font-family'    => 'Open Sans',
                      'font-size'      => '20',
                      'line-height'    => '20',
                      'letter-spacing' => '-1',
                    //   'text-align'     => 'left',
                      'subset'         => 'latin-ext',
                      'type'           => 'google',
                      'unit'           => 'px',
                    ),
                  ),


                  array(
                    'id'      => 'pixmatic-h4-typography',
                    'type'    => 'typography',
                    'title'   => 'H4 Typography',
                    'output'  => 'h4',
                    'default' => array(
                      'color'          => '#212529',
                      'font-family'    => 'Open Sans',
                      'font-size'      => '20',
                      'line-height'    => '20',
                      'letter-spacing' => '-1',
                    //   'text-align'     => 'left',
                      'subset'         => 'latin-ext',
                      'type'           => 'google',
                      'unit'           => 'px',
                    ),
                  ),


                  array(
                    'id'      => 'pixmatic-h5-typography',
                    'type'    => 'typography',
                    'title'   => 'H5 Typography',
                    'output'  => 'h5',
                    'default' => array(
                      'color'          => '#212529',
                      'font-family'    => 'Open Sans',
                      'font-size'      => '20',
                      'line-height'    => '20',
                      'letter-spacing' => '-1',
                    //   'text-align'     => 'left',
                      'subset'         => 'latin-ext',
                      'type'           => 'google',
                      'unit'           => 'px',
                    ),
                  ),


                  array(
                    'id'      => 'pixmatic-h6-typography',
                    'type'    => 'typography',
                    'title'   => 'H6 Typography',
                    'output'  => 'h6',
                    'default' => array(
                      'color'          => '#212529',
                      'font-family'    => 'Open Sans',
                      'font-size'      => '20',
                      'line-height'    => '20',
                      'letter-spacing' => '-1',
                    //   'text-align'     => 'left',
                      'subset'         => 'latin-ext',
                      'type'           => 'google',
                      'unit'           => 'px',
                    ),
                  ),

                  array(
                    'id'      => 'pixmatic-paragraph-typography',
                    'type'    => 'typography',
                    'title'   => 'Paragraph Typography',
                    'output'  => 'p',
                    'default' => array(
                      'color'          => '#212529',
                      'font-family'    => 'Lora',
                      'font-size'      => '18',
                      'line-height'    => '38',
                      'letter-spacing' => '-1',
                    //   'text-align'     => 'left',
                      'subset'         => 'latin-ext',
                      'type'           => 'google',
                      'unit'           => 'px',
                    ),
                  ),
                  
        
            )
            ) );
        

            CSF::createSection( $prefix, array(
            'title'  => 'Color Controls',
            'fields' => array(

                array(
                    'id'          => 'pixmatic-heading-bg-color',
                    'type'        => 'color',
                    'title'       => 'Heading Background Color',
                    'default' => '#ffbc00',
                    'output'      => 'header.masthead',
                    'output_mode' => 'background-color'
                ),

                array(
                    'id'          => 'pixmatic-link-color',
                    'type'        => 'color',
                    'title'       => 'Link Color',
                    'default' => '#ffbc00',
                    'output'      => 'a',
                    'output_mode' => 'color'
                ),

                array(
                    'id'          => 'pixmatic-link-hover-color',
                    'type'        => 'color',
                    'title'       => 'Link Hover Color',
                    'default' => '#ffbc00',
                    'output'      => 'a:hover',
                    'output_mode' => 'color'
                ),
                  
                        
            )
            ) );



            CSF::createSection( $prefix, array(
                'title'  => 'Other Controls',
                'fields' => array(
    
                    array(
                        'id'      => 'pixmatic-custom-favicon',
                        'type'    => 'media',
                        'title'   => 'Custom Favicon',
                        'library' => 'image',
                        'url' => true,
                      ),
                      
                            
                )
                ) );
        
        }
  